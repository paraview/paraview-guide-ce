ParaViewGettingStarted Guide README

The ParaView Getting Started Guide is a Microsoft Word file. Word
offered the best control over page layout over some other
tools such as LaTeX and Open Office.

The PDF file was generated on a Mac with OS X 10.11.4 with Microsoft Word
for Mac version 15.16 (151105).
